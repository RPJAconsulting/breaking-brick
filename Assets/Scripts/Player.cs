using UnityEngine;
using Mirror;

public class Player : NetworkBehaviour
{
    public float speed = 1500;
    public Rigidbody2D rigidbody2d;
    [SyncVar]
    public bool isPlayer1;
    public Color p1color;
    public Color p2color;


    void Start()
	{
        if (isPlayer1)
        {
            gameObject.GetComponent<SpriteRenderer>().color = p1color;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = p2color;
        }

    }

    // need to use FixedUpdate for rigidbody
    void FixedUpdate()
    {
        // only let the local player control the racket.
        // don't control other player's rackets
        if (isLocalPlayer)
		{
            rigidbody2d.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), 0) * speed * Time.fixedDeltaTime;
		}
           
    }
}
