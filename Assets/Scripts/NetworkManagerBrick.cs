using UnityEngine;
using Mirror;

[AddComponentMenu("")]
public class NetworkManagerBrick : NetworkManager
{
	public Transform player1;
	public Transform player2;
	GameObject ball;
	public GameObject brick;

	public ChangeColor scriptCC;

	public brickManager bm;

	public GameObject ballpref;

	public override void OnServerAddPlayer(NetworkConnection conn)
	{
		// add player at correct spawn position
		Transform start = numPlayers == 0 ? player1 : player2;
		GameObject player = Instantiate(playerPrefab, start.position, start.rotation);

		if (numPlayers == 0)
		{
			player.GetComponent<Player>().isPlayer1 = true;
		}
		else
		{
			player.GetComponent<Player>().isPlayer1 = false;

		}

		NetworkServer.AddPlayerForConnection(conn, player);

		// spawn ball if two players
		if (numPlayers == 2)
		{
			bm.ResetLevel(brick);
			ball = Instantiate(spawnPrefabs.Find(prefab => prefab.name == ballpref.name));
			NetworkServer.Spawn(ball);

		}
	}

	public override void OnServerDisconnect(NetworkConnection conn)
	{
		// destroy ball
		if (ball != null)
			NetworkServer.Destroy(ball);

		bm.viderLvl();

		// call base functionality (actually destroys the player)
		base.OnServerDisconnect(conn);
	}


	public void debugColorPlayer()
	{
		//if (Input.GetKeyDown("space"))
		//{
		//	GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().color = p2color;

		//	print("space key was pressed");
		//}
	}

	public void debugLesBrick()
	{
		if (Input.GetKeyDown("space"))
		{
			if (GameObject.FindGameObjectWithTag("brick"))
			{
				GameObject.FindGameObjectWithTag("brick").GetComponent<brick>().Bcollision();
			}

			print("space key was pressed");
		}
	}

	void Update()
	{
		//debugColorPlayer();
		//debugLesBrick();
	}
}