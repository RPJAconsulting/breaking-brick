﻿using UnityEngine;
using Mirror;

public class ChangeColor : NetworkBehaviour
{

    [Command(ignoreAuthority = true)]
	public void CmdChangeColor(GameObject player, Color playerColor)
	{
		player.GetComponent<SpriteRenderer>().color = playerColor;
	}
}
