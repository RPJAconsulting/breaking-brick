﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paddle : MonoBehaviour
{
    public float speed = 15f;
    public bool isPlayer1;


    private float input;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (isPlayer1)
		{
            input = Input.GetAxisRaw("Horizontal");
		}
		else
		{
            input = Input.GetAxisRaw("Vertical");
        }
        
    }

    private void FixedUpdate()
	{
        GetComponent<Rigidbody2D>().velocity = Vector2.right * input * speed;

    }
}
