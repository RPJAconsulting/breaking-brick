﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UIElements;

public class brickManager : NetworkBehaviour
{
    public int rows;
    public int collumns;
    public float spacing;
    //public GameObject brickPrefab;
    public NetworkManagerBrick nm;


    public List<GameObject> bricks = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        //ResetLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetLevel(GameObject _brick)
	{
        viderLvl();

		for (int x = 0; x < collumns; x++)
		{
			for (int y = 0; y < rows; y++)
			{
                Vector2 spawnPos = (Vector2)transform.position + new Vector2(
                    x * (_brick.transform.localScale.x + spacing),
                    -y * (_brick.transform.localScale.y + spacing)
                    );
                GameObject brick = Instantiate(_brick, spawnPos, Quaternion.identity);

                NetworkServer.Spawn(brick);
                //bricks.Add(brick);
			}
		}
	}

    public void RemoveBrick(brick brik)
	{
        //bricks.Remove(brik.gameObject);
        NetworkServer.Destroy(brik.gameObject);
        Debug.Log(GameObject.FindGameObjectsWithTag("brick").Length);
        if (GameObject.FindGameObjectsWithTag("brick").Length == 1)
		{
            Debug.Log("reseting bitch");
            ResetLevel(nm.brick);
		}
	}

    public void viderLvl()
	{
		//foreach (GameObject brick in bricks)
		//{
		//    //Destroy(brick);
		//    NetworkServer.Destroy(gameObject);
		//}
		//bricks.Clear();

		if (GameObject.FindGameObjectsWithTag("brick").Length !=0)
		{
			foreach (GameObject item in GameObject.FindGameObjectsWithTag("brick"))
			{
                NetworkServer.Destroy(item);
            }
		}
    }
}
