# Breaking Brick ! A P2P game !

A small Unity game made by Bouchier Tristan & Prangère Romain to showcase the uses
of peer to peer networking in Unity. We'r using the Mirror framework to allow two
player to play this game simultaneously.

![Demo Scene](Docs/Images/Image1.PNG)

## Changelog

 - 2020-11-10
   - ...
 - 2020-11-10
   - First Version
   
## To play the game

To be able to simply play the game without bothering yourself you can just:

 1. Download the last build of the game [from our drive](https://drive.google.com/u/0/uc?id=1OL9PUVRntnvjazYsjLpuR5fyZXTv97q5&export=download).
 2. Unzipp the files downloaded from the drive on your computer.
 3. Run two instances of the breakingbrick.exe. This way you'll have two instances of the game.
 4. Hit Host on one of them and client on the second to test localy.
 5. If you are doing port fowarding and want to play on two differents computers you can just enter the IP adress for the host and the client on the startup menu of the game.

nb : If you wish to play the game in windowed mode you can launch the game from the .bat file instead of the .exe. 

![Unity Configuration](Docs/Images/Image2.jpg)

## Configuration for Unity

To be able to use this project you'll have to set it up first:

 1. Download the original sources and unversionned files [from our drive](https://drive.google.com/u/0/uc?id=1OL9PUVRntnvjazYsjLpuR5fyZXTv97q5&export=download).
 2. Initiate this git repository somewhere on your computer.
 3. Unzipp the files downloaded from the drive (1) inside the git repository (2) initiated.
 4. Pull master inside the repository to erase the folder with the lasts modifications.
 5. Start unity, locate the folder and it should run.
 
## Testing locally

To test locally without another computer you can create a standalone build:

 1. In the menu bar click on `File > Build Settings...`
 2. Click the `Build` button, choose a destination folder and name your executable.
 3. In the Explorer window that popped up right-click on the `.exe` and select `Create shortcut`
 4. Click `Ok`
 5. Run the executable by double-clicking the shortcut
 6. Hit `Play` in the Unity Editor (or run the `.exe` instead of the shortcut)
 7. This way you'll have two instance. Hit Host on one of them and client on the second.


## TODO
 
- Change the readme link with the proper build folder to play the game directly from the last build ( under section To play the game )
- Start to work on art
- Add a points counter feature
- Syncronise with better lerping the player palets